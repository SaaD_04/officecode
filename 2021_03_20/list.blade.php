<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Team Management Platform</title>
    @include('backend.layouts.styles')
    <link rel="stylesheet" href="{{asset('backend/assets/templates/vendors/font-awesome/css/font-awesome.min.css')}}">
</head>
<body>
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    @include('backend.layouts.header')
    <div class="container-fluid page-body-wrapper">
        @include('backend.layouts.sidebar')
        <div class="main-panel">
            <div class="content-wrapper" style="background-color: whitesmoke;">
                <h3>Team Management</h3>
                <hr class="Dash-Line">
                @include('backend.layouts.messages')
                <div class="DataTableBox" style="margin-top: 40px; max-width: 100%; height: auto;">
                    <div class="row" style="margin-left: 10px;">
                        <div class="col-sm-9" style="margin-top: 20px;">
                            <div class="form-inline">
                                <span>All Team List</span>
                                <input class="form-control mr-sm-2 " type="search" id="search" placeholder="Search..." aria-label="Search" style="margin-left: 10px;">
                            </div>
                        </div>
                        <div class="col-sm-3" style="margin-top: 20px;">
                            <a href="{{route('teams.export')}}"><svg xmlns="http://www.w3.org/2000/svg" width="100" height="34.253" viewBox="0 0 116.299 34.253">
                                    <defs>
                                        <style>
                                            .cls-3{fill:#576271}
                                        </style>
                                    </defs>
                                    <g id="Export_Btn_with_Icon" transform="translate(-1712.137 -1010)">
                                        <g id="Rectangle_57" fill="#fbfbfb" stroke="#586372" transform="translate(1712.137 1010)">
                                            <rect width="116.299" height="34.253" stroke="none" rx="7"/>
                                            <rect width="115.299" height="33.253" x=".5" y=".5" fill="none" rx="6.5"/>
                                        </g>
                                        <text id="Export" fill="#576271" font-family="OpenSans-Regular, Open Sans" font-size="17px" transform="translate(1776.711 1033)">
                                            <tspan x="-25.994" y="0">Export</tspan>
                                        </text>
                                        <g id="Export_Icon" transform="translate(1724.555 1018.585)">
                                            <path id="Path_503" d="M29.983 1.2a4.116 4.116 0 0 0-5.814 0l-2.907 2.909a.632.632 0 1 0 .894.891l2.907-2.9a2.846 2.846 0 0 1 4.025 4.025l-3.8 3.8a2.85 2.85 0 0 1-4.025 0 .632.632 0 1 0-.894.894 4.116 4.116 0 0 0 5.814 0l3.8-3.8a4.121 4.121 0 0 0 0-5.814z" class="cls-3" transform="translate(-13.801 -.001)"/>
                                            <path id="Path_504" d="M8.582 24.917l-2.46 2.46A2.846 2.846 0 0 1 2.1 23.352l3.578-3.578a2.857 2.857 0 0 1 4.025 0 .632.632 0 0 0 .894-.894 4.116 4.116 0 0 0-5.814 0L1.2 22.458a4.111 4.111 0 0 0 5.814 5.813l2.46-2.46a.632.632 0 0 0-.894-.894z" class="cls-3" transform="translate(-.003 -12.087)"/>
                                        </g>
                                    </g>
                                </svg></a>
                            <a href="{{route('teams.create')}}"><button style="border-radius: 5px; border-color: #ec008c; color:#ec008c;">New Team</button></a>
                        </div>
                    </div>
                    <hr>
                    <div style="height: 100%; width: 100%; display: flex; align-items: center; justify-content: center">
                        @foreach($teams as $team)
                        <div class="card" style="width: 200px; height: 270px; display: flex; align-items: center; justify-content: center; background-color: #FEEDFE; margin: 2px">
                            <img src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/nature-quotes-1557340276.jpg?crop=1.00xw:0.757xh;0,0.0958xh&resize=980:*" style="width: 40px; height: 40px; border-radius: 50%">

                            <div style="text-align: center; margin-top:20px">
                                <p>{{$team->name}}</p>
                            </div>
                            <div style="text-align: center; margin-top: 10px">
                                <p style="color: #F009F0; font-size:22px"> <b>65 / 70</b></p>
                                <p style="margin-top: -5px">Task completed</p>
                            </div>
                            <div style="height: 50px; margin-top:10px">
                                @for ($x=0; $x<6; $x++)
                                    <img src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/nature-quotes-1557340276.jpg?crop=1.00xw:0.757xh;0,0.0958xh&resize=980:*" style="width: 20px; height: 20px; border-radius: 50%; margin-left: -10px">
                                @endfor
                            </div>
                            <a href="#" style="color: #F009F0">View details</a>
                        </div>
                    @endforeach
                    </div>
                </div>
                <!-- content-wrapper ends -->
                <!-- partial:partials/_footer.html -->
            {{--            footer--}}
            <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
    </div>
</div>
<!-- page-body-wrapper ends -->
<!-- container-scroller -->

@include('backend.layouts.scripts')
<script>
    $(document).ready(function() {
        const tableCustom =  $('.table').DataTable({
            "pageLength": 4,
            "dom": 'lrtip',
            "lengthChange": false
        });
        $('#search').keyup(function(){
            tableCustom.search($(this).val()).draw() ;
        })

    });
</script>

</body>

</html>
